﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;

namespace inventory.Common
{
    public class Utility
    {
        public static SqlConnection GetConnection()
        {
            var securityPrinciple = (System.Security.Claims.ClaimsPrincipal)HttpContext.Current.User;
            if (securityPrinciple.Claims.Where(x => x.Type == "companyDBName").Count() > 0)
                WebApiApplication.connectionDBName = securityPrinciple.Claims.Where(x => x.Type == "companyDBName").FirstOrDefault().Value;

            if (WebApiApplication.connectionDBName == null)
                return new SqlConnection(ConfigurationManager.ConnectionStrings["DefaultConnection"].ConnectionString);
            else
            {
                SqlConnectionStringBuilder builder = new SqlConnectionStringBuilder(ConfigurationManager.ConnectionStrings["DefaultConnection"].ConnectionString);
                string user = builder.UserID;
                string pass = builder.Password;
                string credentials = user != "" && pass != "" ? (";User=" + user + "; Password = " + pass) : ";Integrated Security=true;";
                return new SqlConnection("Data Source=" + builder.DataSource + ";Initial Catalog=" + WebApiApplication.connectionDBName + credentials);
            }

        }

        #region mapper function new
        public static Guid? MapGuid(IDataRecord reader, string columnName)
        {
            if (HasColumn(reader, columnName))
            {
                if (reader[columnName] == DBNull.Value)
                {
                    return null;
                }
                else
                {
                    return reader.GetGuid(reader.GetOrdinal(columnName));
                }
            }
            return null;
        }

       
        public static string MapString(IDataRecord reader, string columnName)
        {
            if (HasColumn(reader, columnName))
            {

                if (reader[columnName] == DBNull.Value)
                {
                    return null;
                }
                else
                {
                    return reader[columnName].ToString();
                }
            }
            return null;
        }

        public static char? MapChar(IDataRecord reader, string columnName)
        {
            if (HasColumn(reader, columnName))
            {

                if (reader[columnName] == DBNull.Value)
                {
                    return null;
                }
                else
                {
                    return Convert.ToChar(reader[columnName]);
                }
            }
            return null;
        }

        public static char? MapVarchar(IDataRecord reader, string columnName)
        {
            if (HasColumn(reader, columnName))
            {
                if (reader[columnName] == DBNull.Value)
                {
                    return null;
                }
                else
                {
                    var value = reader.GetValue(reader.GetOrdinal(columnName));
                    return Char.Parse(value.ToString());
                }
            }
            return null;
        }

        public static bool MapBool(IDataRecord reader, string columnName)
        {
            if (HasColumn(reader, columnName))
            {
                if (reader[columnName] == DBNull.Value)
                {
                    return false;
                }
                else
                {
                    return (bool)reader[columnName];
                }
            }
            return false;
        }

        public static int? MapInt(IDataRecord reader, string columnName)
        {
            if (HasColumn(reader, columnName))
            {
                if (reader[columnName] == DBNull.Value)
                {
                    return null;
                }
                else
                {
                    return reader.GetInt32(reader.GetOrdinal(columnName));
                }
            }
            return null;
        }
        public static int GetSmallIntegerData(SqlDataReader reader, int columnNumber)
        {
            return (!reader.IsDBNull(columnNumber)) ? reader.GetInt16(columnNumber) : default(Int16);
        }

        public static Int16? MapInt16(IDataRecord reader, string columnName)
        {
            if (HasColumn(reader, columnName))
            {
                if (reader[columnName] == DBNull.Value)
                {
                    return null;
                }
                else
                {
                    return reader.GetInt16(reader.GetOrdinal(columnName));
                }
            }
            return null;
        }

        public static short? MapShort(IDataRecord reader, string columnName)
        {
            if (HasColumn(reader, columnName))
            {
                if (reader[columnName] == DBNull.Value)
                {
                    return null;
                }
                else
                {
                    return reader.GetInt16(reader.GetOrdinal(columnName));

                }
            }
            return null;

        }

        public static byte[] MapByte(IDataRecord reader, string columnName)
        {
            if (HasColumn(reader, columnName))
            {
                if (reader[columnName] == DBNull.Value)
                {
                    return default(byte[]);
                }
                else
                {
                    return (byte[])reader[columnName];

                }
            }
            return default(byte[]);
        }

        private static bool HasColumn(IDataRecord dr, string columnName)
        {
            for (int i = 0; i < dr.FieldCount; i++)
            {
                if (dr.GetName(i).Equals(columnName, StringComparison.InvariantCultureIgnoreCase))
                    return true;
            }
            return false;
        }

        internal static float? MapReal(IDataRecord reader, string columnName)
        {
            if (HasColumn(reader, columnName))
            {
                if (reader[columnName] == DBNull.Value)
                {
                    return null;
                }
                else
                {

                    return (float)reader.GetFloat(reader.GetOrdinal(columnName));
                }
            }
            return null;
        }

        public static decimal? MapDecimal(IDataRecord reader, string columnName)
        {
            if (HasColumn(reader, columnName))
            {
                if (reader[columnName] == DBNull.Value)
                {
                    return null;
                }
                else
                {
                    return reader.GetDecimal(reader.GetOrdinal(columnName));
                }
            }
            return null;
        }

        public static DateTime? MapDateTime(IDataRecord reader, string columnName)
        {
            if (HasColumn(reader, columnName))
            {
                // DateTime dt;
                if (reader[columnName] == DBNull.Value)
                {
                    return null;
                }
                else
                {
                    return reader.GetDateTime(reader.GetOrdinal(columnName));
                }
            }
            return null;
        }

        public static T GetValue<T>(object value)
        {
            if (value == null || value == DBNull.Value)
                return default(T);
            else
                return (T)value;
        }
       
        public static float? MapFloat(IDataRecord reader, string columnName)
        {
            if (HasColumn(reader, columnName))
            {
                if (reader[columnName] == DBNull.Value)
                {
                    return null;
                }
                else
                {
                    return (float?)reader.GetDouble(reader.GetOrdinal(columnName));
                }
            }
            return null;
        }

        #endregion

         
        public static object GetSQLParameterValue(object obj)
        {
            return obj ?? DBNull.Value;
        }
    }
}