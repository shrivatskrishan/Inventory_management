﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web;
using System.Web.Http;
using System.Web.Http.Controllers;

namespace inventory.Common
{
    [System.AttributeUsage(System.AttributeTargets.All, Inherited = true)]
    public class RestAuthorize : AuthorizeAttribute
    {
        public static string AuthStatus { get; set; }

        protected override bool IsAuthorized(HttpActionContext actionContext)
        {
            if (actionContext.Request.Headers.Referrer != null && actionContext.Request.Headers.Referrer.ToString().Contains("swagger"))
            {
                try
                {
                    List<string> query = actionContext.Request.RequestUri.Query.Split('=').ToList();
                    //WebApiApplication.connectionDBName = query[query.Count - 1];

                }
                catch (Exception)
                {

                    throw new Exception(HttpStatusCode.NotFound.ToString(), new Exception(JsonConvert.SerializeObject(new Exception("Please specify company Name"))));
                }

                return true;
            }

            if (actionContext.Request.Headers.Contains("restSId"))
            {
                return sessionIdValidation(actionContext);
            }
            else
            {
               // WebApiApplication.connectionDBName = null;
                return true;
            }
        }

        private bool sessionIdValidation(HttpActionContext actionContext)
        {
            var headers = actionContext.Request.Headers;
            string headerString = System.Text.Encoding.UTF8.GetString(Convert.FromBase64String(headers.GetValues("restSId").First()));
            // 0 = userid, 1 = machine name, 2 = fingerprint
            string[] tokenArray = headerString.Split('|');
            if ((tokenArray[0] == headers.GetValues("userId").First())
              &&
              (tokenArray[1] == System.Environment.MachineName)){
                return true;
            }
            //  &&
            //  (tokenArray[2] == headers.GetValues("fp").First()))
            //{
            //    //WebApiApplication.userId = headers.GetValues("userId").First();
            //    //WebApiApplication.sessionId = headers.GetValues("restSId").First();
            //    //WebApiApplication.sessionId = tokenArray[3];
            //  // WebApiApplication.connectionDBName = headers.GetValues("dbn").First();
            //    return true;
            //}
            else
            {
              //  WebApiApplication.connectionDBName = null;
                return false;
            }
        }

        protected override void HandleUnauthorizedRequest(HttpActionContext filterContext)
        {
            AuthStatus = "failed";
            filterContext.Response = filterContext.Request.CreateErrorResponse(HttpStatusCode.NotFound, "Access Restricted");
        }

    }
}