﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Net;
using System.Web;

namespace inventory.Common
{
    public class Response<T>
    {
        public bool Success;
        public HttpStatusCode Status;
        public string Message;
        public string DeveloperMessage;
        public string Error;
        public T Result;
        public string Version;
        public string NextLink;
        private HttpContext current;
        public string scalarResponseId;
        public int? Total;

        public object TreeJson { get; internal set; }

        public Response()
        {
            this.Version = ConfigurationManager.AppSettings["appVersion"].ToString();
        }

        public Response(HttpContext current)
        {
            this.Version = ConfigurationManager.AppSettings["appVersion"].ToString();
            this.current = current;
        }

		//public void CreateSuccessResponse(HttpStatusCode status, object inventoryGetSuccess1, object inventoryGetSuccess2, object inventory)
		//{
		//	this.Success = true;
		//	this.Status = status.ToString() != null ? status : HttpStatusCode.OK;
		//	this.Message = message;
		//	this.Result = result;
		//	this.DeveloperMessage = developerMessage;
		//	this.Total = total;
		//}

			public void CreateSuccessResponse(HttpStatusCode status, string developerMessage, string message, T result, int? total = 0)
        {
            this.Success = true;
            this.Status = status.ToString() != null ? status : HttpStatusCode.OK;
            this.Message = message;
            this.Result = result;
            this.DeveloperMessage = developerMessage;
            this.Total = total;
        }

        public void CreateResponseHeader(HttpStatusCode status, string nextLinkId = null)
        {
            if (this.current != null)
            {
                if (nextLinkId != null)
                {
                    this.current.Response.AddHeader("Id", nextLinkId);
                    this.current.Response.AddHeader("NextLink", this.current.Request.Url.AbsolutePath + '/' + nextLinkId);
                }
                this.current.Response.AddHeader("Status", status.ToString());
            }
        }

        public void CreateFailResponse(HttpStatusCode status, string developerMessage, string error, T result)
        {
            this.Success = false;
            this.Status = status.ToString() != null ? status : HttpStatusCode.InternalServerError;
            this.Error = error;
            this.Result = result;
            this.DeveloperMessage = developerMessage;
        }

        public void CreateResponseMessage(string developerMessage)
        {
            this.DeveloperMessage = developerMessage;
        }

    }
}