﻿using inventory.Common;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;

namespace inventory.Modules.Product
{
	public class ProductMapper
	{
		internal SqlCommand productSqlQueryParameter(SqlCommand command, ProductEntity productEntity)
		{
			command.Parameters.AddWithValue("@Id", Utility.GetSQLParameterValue(productEntity.Id));
			command.Parameters.AddWithValue("@ProductName", Utility.GetSQLParameterValue(productEntity.ProductName));
			command.Parameters.AddWithValue("@Description", Utility.GetSQLParameterValue(productEntity.Description));
			command.Parameters.AddWithValue("@Discount", Utility.GetSQLParameterValue(productEntity.Discount));
			command.Parameters.AddWithValue("@Cod", Utility.GetSQLParameterValue(productEntity.Cod));
			command.Parameters.AddWithValue("@Sku", Utility.GetSQLParameterValue(productEntity.Sku));

			return command;
		}

		internal ProductEntity mapProductRecord(IDataRecord reader)
		{
			var product = new ProductEntity();
			product.Id = Utility.MapGuid(reader, "ID").Value;
			product.ProductName = Utility.MapString(reader, "NAME");
			product.Description = Utility.MapString(reader, "DESCRIPTION");
			product.Discount = Utility.MapDecimal(reader, "DISCOUNT");
			product.Cod = Utility.MapChar(reader, "COD");
			product.Sku = Utility.MapString(reader, "SKU");

			return product;
		}


		internal SqlCommand variantSqlQueryParameter(SqlCommand command, VariantEntity variantEntity)
		{
			command.Parameters.AddWithValue("@Id", Utility.GetSQLParameterValue(variantEntity.Id));
			command.Parameters.AddWithValue("@ProductId", Utility.GetSQLParameterValue(variantEntity.ProductId));
			command.Parameters.AddWithValue("@VariantName", Utility.GetSQLParameterValue(variantEntity.VariantName));
			return command;
		}

		internal VariantEntity mapvariantRecord(IDataRecord reader)
		{
			var variant = new VariantEntity();
			variant.Id = Utility.MapGuid(reader, "ID").Value;
			variant.ProductId = Utility.MapGuid(reader, "PRODUCT_ID").Value;
			variant.VariantName = Utility.MapString(reader, "VARIANT_NAME");
			return variant;
		}

		internal SqlCommand tagSqlQueryParameter(SqlCommand command, TagEntity tagEntity)
		{
			command.Parameters.AddWithValue("@Id", Utility.GetSQLParameterValue(tagEntity.Id));
			command.Parameters.AddWithValue("@VaraiantId", Utility.GetSQLParameterValue(tagEntity.VaraiantId));
			command.Parameters.AddWithValue("@TagName", Utility.GetSQLParameterValue(tagEntity.TagName));
			return command;
		}

		internal TagEntity maptagRecord(IDataRecord reader)
		{
			var tag = new TagEntity();
			tag.Id = Utility.MapGuid(reader, "ID").Value;
			tag.VaraiantId = Utility.MapGuid(reader, "VARIANT_ID").Value;
			tag.TagName = Utility.MapString(reader, "TAG_NAME");
			return tag;
		}

		internal SqlCommand shippingSqlQueryParameter(SqlCommand command, ShippingEntity shippingEntity)
		{
			command.Parameters.AddWithValue("@Id", Utility.GetSQLParameterValue(shippingEntity.Id));
			command.Parameters.AddWithValue("@ProductId", Utility.GetSQLParameterValue(shippingEntity.ProductId));
			command.Parameters.AddWithValue("@ShippingMethod", Utility.GetSQLParameterValue(shippingEntity.ShippingMethod));
			command.Parameters.AddWithValue("@ShippingPrice", Utility.GetSQLParameterValue(shippingEntity.ShippingPrice));
			return command;
		}

		internal ShippingEntity mapshippingRecord(IDataRecord reader)
		{
			var shipping = new ShippingEntity();
			shipping.Id = Utility.MapGuid(reader, "ID").Value;
			shipping.ProductId = Utility.MapGuid(reader, "PRODUCT_ID").Value;
			shipping.ShippingMethod = Utility.MapString(reader, "SHIPPING_METHOD");
			shipping.ShippingPrice = Utility.MapDecimal(reader, "SHIPPING_PRICE").Value;

			return shipping;
		}
		
	
		internal ProductViewModel mapProductInventoryRecord(IDataRecord reader)
		{
			var product = new ProductViewModel();

			product.ProductName = Utility.MapString(reader, "PRODUCT_NAME");
			product.VariantName = Utility.MapString(reader, "VARIANT");
			product.TagName = Utility.MapString(reader, "TAG_NAME");
			product.Discount = Utility.MapDecimal(reader, "DISCOUNT").Value;
			product.Sku = Utility.MapString(reader, "SKU");
			product.ProductDescription = Utility.MapString(reader, "PRODUCT_DESCRIPTION");
			product.ShippingMethod = Utility.MapString(reader, "SHIPPING_METHOD");
			product.ShippingPrice = Utility.MapDecimal(reader, "SHIPPING_PRICE").Value;
			product.Cod = Utility.MapChar(reader, "COD").Value;

			return product;
		}

		//internal SqlCommand VariantViewModelQueryParameter(SqlCommand command, VariantViewModel variantViewModel)
		//{
		//	command.Parameters.AddWithValue("@Variantname", Utility.GetSQLParameterValue(variantViewModel.Variantname));
		//	command.Parameters.AddWithValue("@Tag", Utility.GetSQLParameterValue(variantViewModel.Tag));

		//	return command;
		//}


	}
}