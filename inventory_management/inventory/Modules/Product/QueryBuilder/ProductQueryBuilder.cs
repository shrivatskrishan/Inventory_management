﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace inventory.Modules.Product
{
	public class ProductQueryBuilder
	{
       internal string getInventoryDataQuery()
		{
			return @"SELECT
       NAME AS PRODUCT_NAME,
	   Variant.VARIANT_NAME as VARIANT,
	   Tag.TAG_NAME,
	   DISCOUNT,
	   SKU,
	   DESCRIPTION AS PRODUCT_DESCRIPTION,
	   SHIPPING_METHOD,
	   SHIPPING_PRICE,
	   COD
  FROM  
       Product Product
	   INNER JOIN Variant Variant ON (Variant.PRODUCT_ID=Product.ID)
	   INNER JOIN Shipping Shipping ON (Shipping.PRODUCT_ID = Product.ID)
	   INNER JOIN Tag Tag ON (Tag.VARIANT_ID = Variant.ID)";
		}

        #region product
		internal string getProductdataquery()
		{
			return @"SELECT * FROM Product";
		}


		internal string saveproductquery()
		{
			return @"INSERT INTO Product
							(
								ID, 
								NAME, 
								DESCRIPTION, 
								DISCOUNT,
                                COD,
                                SKU
							)
						VALUES
							(
								@Id, 
								@ProductName, 
								@Description, 
								@Discount,
                                @Cod,
                                @Sku
							)";
		}



		internal string Updateproductquery()
		{
			return @"UPDATE Product
						SET 
							NAME = @ProductName, 
						    DESCRIPTION= @Description, 
							DISCOUNT= @Discount,
                            COD=@Cod,
                            SKU=@Sku
                            
						WHERE 
							ID = @Id";
		}

		internal string deleteproductQuery()
		{
			return @" DELETE FROM Product WHERE ID = @Id ";
		}

		#endregion

		#region variant

		internal string getvariant()
		{
			return @"SELECT * FROM Variant";
		}

		internal string savevariantquery()
		{
			return @"INSERT INTO Variant
							(
								ID, 
								PRODUCT_ID, 
								VARIANT_NAME
								
							)
						VALUES
							(
								@Id, 
								@ProductId, 
								@VariantName 
							
							)";
		}

		internal string Updatevariantquery()
		{
			return @"UPDATE Variant
                      SET
                       ID=@Id
                       PRODUCT_ID=@ProductId, 
                       VARIANT_NAME=@VariantName  
                                   
                    WHERE
                      ID=@Id";
		}

		internal string deletevariantQuery()
		{
			return @" DELETE FROM Variant WHERE ID = @Id ";
		}
		#endregion

		#region tag

		internal string gettag()
		{
			return @"SELECT ID, VARIANT_ID,TAG_NAME FROM Tag";
		}


		internal string savetagquery()
		{
			return @"INSERT INTO Tag
							(
								ID, 
								VARIANT_ID, 
								TAG_NAME
								
							)
						VALUES
							(
								@Id, 
								@VaraiantId, 
								@TagName 
							
							)";
		}



		internal string Updatetagquery()
		{
			return @"UPDATE Tag
                      SET
                       VARIANT_ID = @VaraiantId, 
                       TAG_NAME = @TagName  
                                   
                    WHERE
                      ID=@Id";
		}


		internal string deletetagQuery()
		{
			return @" DELETE FROM Tag WHERE ID = @Id ";
		}

		#endregion


		#region shipping

		internal string getshipping()
		{
			return @"SELECT * FROM Shipping";
		}

		internal string saveshippingquery()
		{
			return @"INSERT INTO Shipping
							(
								ID, 
								PRODUCT_ID, 
								SHIPPING_METHOD,
                                SHIPPING_PRICE
								
							)
						VALUES
							(
								@Id, 
								@ProductId, 
								@ShippingMethod,
                                @ShippingPrice
							
							)";

		}

		internal string Updateshippingquery()
		{
			return @"UPDATE Shipping
                      SET
                                ID=@Id, 
								PRODUCT_ID= @ProductId,
								SHIPPING_METHOD=@ShippingMethod,
                                SHIPPING_PRICE=@ShippingPrice
                                   
                    WHERE
                      ID=@Id";
		}


		internal string deleteshippingQuery()
		{
			return @" DELETE FROM Shipping WHERE ID = @Id ";
		}
		#endregion


	}
}