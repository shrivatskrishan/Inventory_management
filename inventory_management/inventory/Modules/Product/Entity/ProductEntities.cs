﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace inventory.Modules.Product
{
	public class ProductEntity
	{
		/// <summary>
		/// Get or Set the Id
		/// </summary>
		public Guid Id { get; set; }

		/// <summary>
		/// 
		/// </summary>
		public string ProductName { get; set; }

		/// <summary>
		/// 
		/// </summary>
		public string Description { get; set; }

		/// <summary>
		/// 
		/// </summary>
		public decimal? Discount { get; set; }

		/// <summary>
		/// 
		/// </summary>
		public char? Cod { get; set; }

		/// <summary>
		/// 
		/// </summary>
		public string Sku { get; set; }

	}
	public class VariantEntity
	{
		public Guid Id { get; set; }
		public Guid ProductId { get; set; }
		public string VariantName { get; set; }
		
	}
	public class TagEntity
	{
		public Guid Id { get; set; }
		public Guid VaraiantId { get; set; }
		public string TagName { get; set; }
	}
	public class ShippingEntity
	{
		public Guid Id { get; set; }
		public Guid ProductId { get; set; }
		public string ShippingMethod { get; set; }
		public decimal ShippingPrice { get; set; }
	}

	/// <summary>
	/// 
	/// </summary>
	public class ProductViewModel
	{
		public string ProductName { get; set; }
		public string VariantName { get; set; }
		public string TagName { get; set; }
		public decimal Discount { get; set; }
		public string Sku { get; set; }
		public string ProductDescription { get; set; }
		public string ShippingMethod { get; set; }
		public decimal ShippingPrice { get; set; }
		public char Cod { get; set; }
	}

	/// <summary>
	/// 
	/// </summary>
	//public class VariantViewModel
	//{
	//	public string Variantname { get; set; }
	//	public string Tag { get; set; }
	//}
}