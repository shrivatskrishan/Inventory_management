﻿using inventory.Common;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Threading.Tasks;
using System.Web;

namespace inventory.Modules.Product
{
	public class ProductModel
	{

		private ProductQueryBuilder querybuilder;
		private ProductMapper mapper;

		public ProductModel()
		{
			querybuilder = new ProductQueryBuilder();
			mapper = new ProductMapper();
		}


		internal async Task<List<ProductViewModel>> GetInventoryData()
		{
			var inventory = new List<ProductViewModel>();
			using (SqlConnection conn = Utility.GetConnection())
			{
				conn.Open();

				var command = new SqlCommand();
				command = new SqlCommand(querybuilder.getInventoryDataQuery(), conn);
				var reader = command.ExecuteReader();

				while (reader.Read())
				{
					//var count = Utility.MapInt(reader, "RowsCount");
					inventory.Add(mapper.mapProductInventoryRecord(reader));
				}

			}

			return inventory;
		}
        #region product

		internal async Task <List<ProductEntity>> GetProduct()
		{
			var product = new List<ProductEntity>();
			using (SqlConnection conn = Utility.GetConnection())
			{

				conn.Open();

				var command = new SqlCommand();
				command = new SqlCommand(querybuilder.getProductdataquery(), conn);
				var reader = command.ExecuteReader();

				while (reader.Read())
				{
					//var count = Utility.MapInt(reader, "RowsCount");
					product.Add(mapper.mapProductRecord(reader));
				}


			}

			return product;
		}

		internal async Task<Guid?> saveproduct(ProductEntity product)
		{
			using (SqlConnection conn = Utility.GetConnection())
			{
				// conn.ConnectionString = "connection_string";
				conn.Open();
				product.Id = Guid.NewGuid();
				var command = new SqlCommand(querybuilder.saveproductquery(), conn);
				command = mapper.productSqlQueryParameter(command, product);

				if (command.ExecuteNonQuery() < 1)
				{
					throw new Exception("No Content change");
				}
				return product.Id;
			}
		}

		internal async Task Updateproduct(ProductEntity product)
		{
			using (SqlConnection conn = Utility.GetConnection())
			{
				//        conn.ConnectionString = "connection_string";
				conn.Open();
				var command = new SqlCommand(querybuilder.Updateproductquery(), conn);
				command = mapper.productSqlQueryParameter(command, product);

				if (command.ExecuteNonQuery() < 1)
				{
					throw new Exception("No Content change");
				}

			}

		}
		internal async Task Deleteproduct(Guid id)
		{
			using (SqlConnection conn = Utility.GetConnection())
			{
				conn.Open();
				var command = new SqlCommand(querybuilder.deleteproductQuery(), conn);
				command.Parameters.AddWithValue("@Id", id);

				if (command.ExecuteNonQuery() < 1)
				{
					throw new Exception("No Content delete");
				}
			}

		}

		#endregion

		#region variant

		internal async Task <List<VariantEntity>> Getvariant()
		{
			var variant = new List<VariantEntity>();
			using (SqlConnection conn = Utility.GetConnection())
			{
				
				conn.Open();

				var command = new SqlCommand();
				command = new SqlCommand(querybuilder.getvariant(), conn);
				var reader = command.ExecuteReader();

				while (reader.Read())
				{
					//var count = Utility.MapInt(reader, "RowsCount");
					variant.Add(mapper.mapvariantRecord(reader));
				}


			}

			return variant;
		}

		internal async Task<Guid?> savevariant(VariantEntity variantdetail)
		{
			using (SqlConnection conn = Utility.GetConnection())
			{
				// conn.ConnectionString = "connection_string";
				conn.Open();
				variantdetail.Id = Guid.NewGuid();
				var command = new SqlCommand(querybuilder.savevariantquery(), conn);
				command = mapper.variantSqlQueryParameter(command, variantdetail);

				if (command.ExecuteNonQuery() < 1)
				{
					throw new Exception("No Content change");
				}
				return variantdetail.Id;
			}
		}

		internal async Task Updatevariant(VariantEntity variantdetail)
		{
			using (SqlConnection conn = Utility.GetConnection())
			{
				//        conn.ConnectionString = "connection_string";
				conn.Open();
				var command = new SqlCommand(querybuilder.Updatevariantquery(), conn);
				command = mapper.variantSqlQueryParameter(command, variantdetail);

				if (command.ExecuteNonQuery() < 1)
				{
					throw new Exception("No Content change");
				}

			}

		}

		internal async Task Deletevariant(Guid id)
		{
			using (SqlConnection conn = Utility.GetConnection())
			{
				conn.Open();
				var command = new SqlCommand(querybuilder.deletevariantQuery(), conn);
				command.Parameters.AddWithValue("@Id", id);

				if (command.ExecuteNonQuery() < 1)
				{
					throw new Exception("No Content delete");
				}
			}

		}



		#endregion

		#region tag


		internal async Task<List<TagEntity>> Gettag()
		{
			var tag = new List<TagEntity>();
			using (SqlConnection conn = Utility.GetConnection())
			{
				conn.Open();

				var command = new SqlCommand();
				command = new SqlCommand(querybuilder.gettag(), conn);
				var reader = command.ExecuteReader();

				while (reader.Read())
				{
					//var count = Utility.MapInt(reader, "RowsCount");
					tag.Add(mapper.maptagRecord(reader));
				}


			}
			return tag;
		}




		


		internal async Task<Guid?> savetag(TagEntity tagdetail)
		{
			using (SqlConnection conn = Utility.GetConnection())
			{
				// conn.ConnectionString = "connection_string";
				conn.Open();
				tagdetail.Id = Guid.NewGuid();
				var command = new SqlCommand(querybuilder.savetagquery(), conn);
				command = mapper.tagSqlQueryParameter(command, tagdetail);

				if (command.ExecuteNonQuery() < 1)
				{
					throw new Exception("No Content change");
				}
				return tagdetail.Id;
			}
		}


		internal async Task Updatetag(TagEntity tagdetail)
		{
			using (SqlConnection conn = Utility.GetConnection())
			{
				//        conn.ConnectionString = "connection_string";
				conn.Open();
				var command = new SqlCommand(querybuilder.Updatetagquery(), conn);
				command = mapper.tagSqlQueryParameter(command, tagdetail);

				if (command.ExecuteNonQuery() < 1)
				{
					throw new Exception("No Content change");
				}

			}

		}


		internal async Task Deletetag(Guid id)
		{
			using (SqlConnection conn = Utility.GetConnection())
			{
				conn.Open();
				var command = new SqlCommand(querybuilder.deletetagQuery(), conn);
				command.Parameters.AddWithValue("@Id", id);

				if (command.ExecuteNonQuery() < 1)
				{
					throw new Exception("No Content delete");
				}
			}

		}

		#endregion

		#region shipping

		internal async Task <List<ShippingEntity>> Getshipping()
		{
			var shipping = new List<ShippingEntity>();
			using (SqlConnection conn = Utility.GetConnection())
			{
				conn.Open();

				var command = new SqlCommand();
				command = new SqlCommand(querybuilder.getshipping(), conn);
				var reader = command.ExecuteReader();

				while (reader.Read())
				{
					//var count = Utility.MapInt(reader, "RowsCount");
					shipping.Add(mapper.mapshippingRecord(reader));
				}


			}
			return shipping;
		}



		internal async Task<Guid?> saveshipping(ShippingEntity shippingdetail)
		{
			using (SqlConnection conn = Utility.GetConnection())
			{
				// conn.ConnectionString = "connection_string";
				conn.Open();
				shippingdetail.Id = Guid.NewGuid();
				var command = new SqlCommand(querybuilder.saveshippingquery(), conn);
				command =  mapper.shippingSqlQueryParameter(command, shippingdetail);

				if (command.ExecuteNonQuery() < 1)
				{
					throw new Exception("No Content change");
				}
				return shippingdetail.Id;
			}
		}


		internal async Task Updateshippig(ShippingEntity shippingdetail)
		{
			using (SqlConnection conn = Utility.GetConnection())
			{
				//        conn.ConnectionString = "connection_string";
				conn.Open();
				var command = new SqlCommand(querybuilder.Updateshippingquery(), conn);
				command = mapper.shippingSqlQueryParameter(command, shippingdetail);

				if (command.ExecuteNonQuery() < 1)
				{
					throw new Exception("No Content change");
				}

			}

		}

		internal async Task Deleteshipping(Guid id)
		{
			using (SqlConnection conn = Utility.GetConnection())
			{
				conn.Open();
				var command = new SqlCommand(querybuilder.deleteshippingQuery(), conn);
				command.Parameters.AddWithValue("@Id", id);

				if (command.ExecuteNonQuery() < 1)
				{
					throw new Exception("No Content delete");
				}
			}

		}


		#endregion



	}
}