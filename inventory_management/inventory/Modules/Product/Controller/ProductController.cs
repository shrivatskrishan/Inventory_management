﻿using inventory.Common;
using System;
using System.Collections.Generic;
using System.Net;
using System.Threading.Tasks;
using System.Web;
using System.Web.Http;

namespace inventory.Modules.Product
{
	[RoutePrefix("api/inventory-management")]
	[RestAuthorize]
	public class ProductController : ApiController
    {
		private ProductModel productmodel;

		public ProductController()
		{
			productmodel = new ProductModel();
		}

		/// <summary>
		/// 
		/// </summary>
		/// <returns></returns>
		[Route("")]
		[HttpGet]
		public async Task<Response<IEnumerable<ProductViewModel>>> GetInventoryData()
		{
			var response = new Response<IEnumerable<ProductViewModel>>();

			var inventory = await productmodel.GetInventoryData();
			response.CreateSuccessResponse(HttpStatusCode.OK, "Success", "Success", inventory);
			return response;
		}

		#region product

		/// <summary>
		/// 
		/// </summary>
		/// <returns></returns>
		[Route("product")]
		[HttpGet]
		public async Task<Response<IEnumerable<ProductEntity>>> GetProduct()
		{
			 var response = new Response<IEnumerable<ProductEntity>>();

			var product = await productmodel.GetProduct();
			response.CreateSuccessResponse(HttpStatusCode.OK, "Success", "Success", product);
			return response;
		}

		/// <summary>
		/// 
		/// </summary>
		/// <param name="productdetail"></param>
		/// <returns></returns>
		[Route("product")]
		[HttpPost]
		public async Task saveproduct([FromBody] ProductEntity product)
		{
			var respone = new Response<Guid?>(HttpContext.Current);
			var retVal = await productmodel.saveproduct(product);
			respone.CreateResponseHeader(HttpStatusCode.Created, retVal.ToString());
		}

		/// <summary>
		/// 
		/// </summary>
		/// <param name="productdetail"></param>
		/// <param name="id"></param>
		/// <returns></returns>
		[Route("product/{id}")]
		[HttpPut]
		public async Task Updateproduct([FromBody] ProductEntity product, Guid id)
		{
			var response = new Response<Guid?>(HttpContext.Current);

			product.Id = id;
			await productmodel.Updateproduct(product);
			response.CreateResponseHeader(HttpStatusCode.OK);
		}


		[Route("product/{id}")]
		[HttpDelete]
		public async Task Deleteproduct(Guid id)
		{
			var response = new Response<Guid?>(HttpContext.Current);
			await productmodel.Deleteproduct(id);
			response.CreateResponseHeader(HttpStatusCode.OK);
		}

		#endregion


		#region variant
		/// <summary>
		/// 
		/// </summary>
		/// <returns></returns>
		[Route("variant")]
		[HttpGet]
		public async Task<Response<IEnumerable<VariantEntity>>> Getvariant()
		{
			var response = new Response<IEnumerable<VariantEntity>>();

			var variant = await productmodel.Getvariant();
			response.CreateSuccessResponse(HttpStatusCode.OK, "Success", "Success", variant);
			return response;
		}
		/// <summary>
		/// 
		/// </summary>
		/// <param name="variantdetail"></param>
		/// <returns></returns>
		[Route("variant")]
		[HttpPost]
		public async Task savevariant([FromBody] VariantEntity variantdetail)
		{
			var respone = new Response<Guid?>(HttpContext.Current);
			var retVal = await productmodel.savevariant(variantdetail);
			respone.CreateResponseHeader(HttpStatusCode.Created, retVal.ToString());
		}
		/// <summary>
		/// 
		/// </summary>
		/// <param name="variantdetail"></param>
		/// <param name="id"></param>
		/// <returns></returns>
		[Route("variant/ {id}")]
		[HttpPut]
		public async Task Updatevariant([FromBody] VariantEntity variantdetail, Guid id)
		{
			var response = new Response<Guid?>(HttpContext.Current);

			variantdetail.Id = id;
			await productmodel.Updatevariant(variantdetail);
			response.CreateResponseHeader(HttpStatusCode.OK);
		}

		[Route("variant/{id}")]
		[HttpDelete]
		public async Task Deletevariant(Guid id)
		{
			var response = new Response<Guid?>(HttpContext.Current);
			await productmodel.Deletevariant(id);
			response.CreateResponseHeader(HttpStatusCode.OK);
		}


		#endregion


		#region tag

		[Route("tag")]
		[HttpGet]
		public async Task<Response<IEnumerable<TagEntity>>> Gettag()
		{
		   var response = new Response<IEnumerable<TagEntity>>();

			var tag = await productmodel.Gettag();
			response.CreateSuccessResponse(HttpStatusCode.OK, "Success", "Success", tag);
			return response;
		}


		[Route("tag")]
		[HttpPost]
		public async Task savetag([FromBody] TagEntity tagdetail)
		{
			var respone = new Response<Guid?>(HttpContext.Current);
			var retVal = await productmodel.savetag(tagdetail);
			respone.CreateResponseHeader(HttpStatusCode.Created, retVal.ToString());
		}

		[Route("tag/{id}")]
		[HttpPut]
		public async Task Updatetag([FromBody] TagEntity tagdetail, Guid id)
		{
			var response = new Response<Guid?>(HttpContext.Current);

			tagdetail.Id = id;
			await productmodel.Updatetag(tagdetail);
			response.CreateResponseHeader(HttpStatusCode.OK);
		}

		[Route("tag/{id}")]
		[HttpDelete]
		public async Task Deletetag(Guid id)
		{
			var response = new Response<Guid?>(HttpContext.Current);
			await productmodel.Deletetag(id);
			response.CreateResponseHeader(HttpStatusCode.OK);
		}


		#endregion


		#region shipping
		[Route("shipping")]
		[HttpGet]
		public async Task<Response<IEnumerable<ShippingEntity>>> Getshipping()
		{
			var response = new Response<IEnumerable<ShippingEntity>>();

			var shipping =await productmodel.Getshipping();
		    response.CreateSuccessResponse(HttpStatusCode.OK, "Success", "Success", shipping);
			return response;
		}

		[Route("shipping")]
		[HttpPost]
		public async Task saveshipping([FromBody] ShippingEntity shippingdetail)
		{
			var respone = new Response<Guid?>(HttpContext.Current);
			var retVal = await productmodel.saveshipping(shippingdetail);
			respone.CreateResponseHeader(HttpStatusCode.Created, retVal.ToString());
		}

		[Route("shipping/{id}")]
		[HttpPut]
		public async Task Updateshippig([FromBody] ShippingEntity shippingdetail, Guid id)
		{
			var response = new Response<Guid?>(HttpContext.Current);

			shippingdetail.Id = id;
			await productmodel.Updateshippig(shippingdetail);
			response.CreateResponseHeader(HttpStatusCode.OK);
		}

		[Route("shipping/{id}")]
		[HttpDelete]
		public async Task Deleteshipping(Guid id)
		{
			var response = new Response<Guid?>(HttpContext.Current);
			await productmodel.Deleteshipping(id);
			response.CreateResponseHeader(HttpStatusCode.OK);
		}


		#endregion









	}
}
