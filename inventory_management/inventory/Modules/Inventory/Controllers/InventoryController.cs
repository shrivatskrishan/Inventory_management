﻿using inventory.Common;
using System;
using System.Collections.Generic;
using System.Net;
using System.Threading.Tasks;
using System.Web;
using System.Web.Http;

namespace inventory.Modules.Inventory
{
    [RoutePrefix("api/inventory-management")]
    [RestAuthorize]
    public class InventoryController : ApiController
    {
        private InventoryModel inventorymodel;

        public InventoryController()
        {
            inventorymodel = new InventoryModel();
        }

		/// <summary>
		/// 
		/// </summary>
		/// <returns></returns>
        [Route("inventory")]
        [HttpGet]
        public async Task<IEnumerable<InventoryEntity>> GetInvetoryData()
        {
            // var response = new Response<IEnumerable<InventoryEntity>>();

            var inventory = inventorymodel.GetInvetoryData();
            // response.CreateSuccessResponse(HttpStatusCode.OK, "Success", "Success", inventory);
            return inventory;
        }

		/// <summary>
		/// 
		/// </summary>
		/// <param name="inventory"></param>
		/// <returns></returns>
        [Route("inventory")]
        [HttpPost]
        public async Task save([FromBody] InventoryEntity inventory)
        {
			var respone = new Response<Guid?>(HttpContext.Current);
            var retVal = await inventorymodel.Save(inventory);
            respone.CreateResponseHeader(HttpStatusCode.Created, retVal.ToString());
        }

		/// <summary>
		/// 
		/// </summary>
		/// <param name="Id"></param>
		/// <returns></returns>
		[Route("inventory/{id}")]
		[HttpGet]
		public async Task<Response<InventoryEntity>> GetinventoryById(Guid Id)
		{
			var response = new Response<InventoryEntity>();
			var inventory = await inventorymodel.GetinventoryById(Id);
			return response;
		}

		/// <summary>
		/// 
		/// </summary>
		/// <param name="inventory"></param>
		/// <param name="id"></param>
		/// <returns></returns>
		[Route("inventory/{id}")]
        [HttpPut]
        public  async Task Update([FromBody] InventoryEntity inventory, Guid id)
        {
			var response = new Response<Guid?>(HttpContext.Current);

			inventory.Id = id;
            await inventorymodel.Update(inventory);
            response.CreateResponseHeader(HttpStatusCode.OK);
         }

		/// <summary>
		/// 
		/// </summary>
		/// <param name="id"></param>
		/// <returns></returns>
		[Route("inventory/{id}")]
	    [HttpDelete]
		public async Task Delete(Guid id)
		{
			var response = new Response<Guid?>(HttpContext.Current);
	        await inventorymodel.Delete(id);
		    response.CreateResponseHeader(HttpStatusCode.OK);
		}

		  

	}
}
