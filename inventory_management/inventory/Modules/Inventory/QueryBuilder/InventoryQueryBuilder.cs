﻿namespace inventory.Modules.Inventory
{
    public class InventoryQueryBuilder
    {
        internal string getInventoryQuery()
        {
            return @"select count(*) OVER() AS RowsCount, * from Inventorydata";
        }

       internal string saveInventory()
        {
            return @"INSERT INTO Inventorydata
							(
								ID, 
								PRODUCT_ID, 
								INVENTORY_NAME, 
								PRICE,
                                INVENTORY,
                                IS_DEFAULT
							)
						VALUES
							(
								@Id, 
								@ProductId, 
								@InventoryName, 
								@Price,
                                @Inventory,
                                @Defaultvalue
							)";
        }
        internal string UpdateInventory()
        {
            return @"UPDATE [dbo].[Inventorydata]
						SET 
							PRODUCT_ID = @ProductId, 
							[INVENTORY_NAME]  = @InventoryName, 
							PRICE	   = @Price,
                            INVENTORY=@Inventory,
                            IS_DEFAULT=@Defaultvalue
                            
						WHERE 
							ID = @Id";
        }
		internal string deleteInventoryQuery()
		{
			return @" DELETE FROM Inventorydata WHERE ID = @Id ";
		}

		internal string getInventorydataByIdQuery()
		{
			return @" SELECT * FROM Inventorydata WHERE ID = @Id ";
		}

	}
}