﻿
using System;
using System.ComponentModel.DataAnnotations;

namespace inventory.Modules.Inventory
{
    public class InventoryEntity
    {
        public Guid Id { get; set; }

        public Guid? ProductId { get; set; }

       // public string name { get; set; }
        [Required]
        public string InventoryName { get; set; }

        public decimal? Price { get; set; }
        
        public int? Inventory { get; set; }
        
        public char? Defaultvalue { get; set; }



    }

}