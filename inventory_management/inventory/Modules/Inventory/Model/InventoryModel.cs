﻿using inventory.Common;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Threading.Tasks;

namespace inventory.Modules.Inventory
{
    /// <summary>
    /// 
    /// </summary>
    public class InventoryModel
    {
        private InventoryQueryBuilder queryBuilder;
        private InventoryMapper mapper;

        /// <summary>
        /// 
        /// </summary>
        public InventoryModel()
        {
            queryBuilder = new InventoryQueryBuilder();
            mapper = new InventoryMapper();
        }

        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
		/// 

        internal List<InventoryEntity> GetInvetoryData()
        {
            var inventory = new List<InventoryEntity>();
            using (SqlConnection conn = Utility.GetConnection())
            {
             conn.Open();
               
                   var command = new SqlCommand();
                   command = new SqlCommand(queryBuilder.getInventoryQuery(), conn);
                   var reader = command.ExecuteReader();

                while (reader.Read())
                {
                    var count = Utility.MapInt(reader, "RowsCount");
                    inventory.Add(mapper.MapInventoryRecord(reader));
                }


            }

                    return inventory;
        }

        internal async Task<Guid?> Save(InventoryEntity inventory)
         {
          using(SqlConnection conn = Utility.GetConnection())
           {
                  conn.Open();
                  inventory.Id = Guid.NewGuid();
                  var command = new SqlCommand(queryBuilder.saveInventory(), conn);
                  command=mapper.SqlQueryParameter(command, inventory);

             if(command.ExecuteNonQuery() < 1)
             {
                   throw new Exception("No Content change");
             }
                   return inventory.Id;
          }
        }

           internal async Task Update(InventoryEntity inventory)
           {
             using (SqlConnection conn = Utility.GetConnection())
             {
               
                   conn.Open();
                   var command = new SqlCommand(queryBuilder.UpdateInventory(), conn);
                   command = mapper.SqlQueryParameter(command, inventory);

                if (command.ExecuteNonQuery() < 1)
                {
                    throw new Exception("No Content change");
                }

             }

           }

           internal async Task Delete(Guid id)
           {
             using (SqlConnection conn = Utility.GetConnection())
             {
                  conn.Open();
                  var command = new SqlCommand(queryBuilder.deleteInventoryQuery(), conn);
                  command.Parameters.AddWithValue("@id", id);

				if (command.ExecuteNonQuery() < 1)
				{
					throw new Exception("No Content delete");
				}
			 }

           }

		  internal async Task<InventoryEntity> GetinventoryById(Guid Id)
		  {
		     var inventorydetail = new InventoryEntity();
		     using (SqlConnection conn = Utility.GetConnection())
		      {
		          conn.Open();
		          var command = new SqlCommand(queryBuilder.getInventorydataByIdQuery(), conn);
		          command.Parameters.AddWithValue("@Id",Id);
		          var reader = command.ExecuteReader();
		        if (reader.Read())
		       {
					
					inventorydetail = mapper.MapInventoryRecord(reader);
		         }
		          return inventorydetail;
		      }
			
		}





	}
}