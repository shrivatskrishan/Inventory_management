﻿using System;
using System.Data;
using System.Data.SqlClient;
using inventory.Common;

namespace inventory.Modules.Inventory
{
    public class InventoryMapper
    {

        internal SqlCommand SqlQueryParameter(SqlCommand command, InventoryEntity inventorydata)
        {
            command.Parameters.AddWithValue("@Id", Utility.GetSQLParameterValue(inventorydata.Id));
            command.Parameters.AddWithValue("@ProductId", Utility.GetSQLParameterValue(inventorydata.ProductId));
            command.Parameters.AddWithValue("@InventoryName", Utility.GetSQLParameterValue(inventorydata.InventoryName));
            command.Parameters.AddWithValue("@Price", Utility.GetSQLParameterValue(inventorydata.Price));
            command.Parameters.AddWithValue("@Inventory", Utility.GetSQLParameterValue(inventorydata.Inventory));
            command.Parameters.AddWithValue("@Defaultvalue", Utility.GetSQLParameterValue(inventorydata.Defaultvalue));

            return command;
        }


        internal InventoryEntity MapInventoryRecord(IDataRecord reader)
       {
          var inventory = new InventoryEntity();
           inventory.Id= Utility.MapGuid(reader, "ID").Value;
           inventory.ProductId = Utility.MapGuid(reader, "PRODUCT_ID");
           inventory.InventoryName = Utility.MapString(reader, "INVENTORY_NAME");
           inventory.Price = Utility.MapDecimal(reader, "PRICE");
           inventory.Inventory = Utility.MapInt(reader, "INVENTORY");
           inventory.Defaultvalue = Utility.MapChar(reader, "IS_DEFAULT");

           return inventory;
         }

        //internal void SqlQueryParameter(SqlCommand command, InventoryEntity inventoryclass)
        //{
        //     command.Parameters.AddWithValue("@Id", Utility.GetSQLParameterValue(inventoryclass.Id));
        //     command.Parameters.AddWithValue("@ProductId", Utility.GetSQLParameterValue(inventoryclass.ProductId));
        //     command.Parameters.AddWithValue("@InventoryName", Utility.GetSQLParameterValue(inventoryclass.InventoryName));
        //     command.Parameters.AddWithValue("@Price", Utility.GetSQLParameterValue(inventoryclass.Price));
        //     command.Parameters.AddWithValue("@Inventory", Utility.GetSQLParameterValue(inventoryclass.Inventory));
        //     command.Parameters.AddWithValue("@Defaultvalue", Utility.GetSQLParameterValue(inventoryclass.Defaultvalue));
        //}



    }
}